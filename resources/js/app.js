import './bootstrap';
import { createApp } from 'vue';

const app = createApp({});

import ExampleComponent from './components/ExampleComponent.vue';
app.component('example-component', ExampleComponent);

import AppComponent from './components/AppComponent.vue';
app.component('app-component', AppComponent);

import router from './router';

app.use(router);
app.mount('#app');
