import { createRouter, createWebHistory } from "vue-router";

import home from '../components/HomeComponent.vue';
import about from '../components/AboutComponent.vue';
import notfound from '../components/NotFoundComponent.vue';

const routes = [
  {
    path: '/',
    component: home
  },
  {
    path: '/about',
    component: about
  },
  {
    path: '/:pathMatch(.*)*',
    component: notfound
  }
]

const router = createRouter({
  history: createWebHistory(),
  linkActiveClass: 'active',
  routes,
})

export default router
